package app.ui.console;

import app.controller.LinearRegressionController;
import app.domain.shared.Constants;
import app.domain.shared.ConstantsLinearRegression;
import app.ui.console.utils.Utils;

import java.util.Date;

public class LinearRegressionUI {

    public LinearRegressionController ctrl;

    public LinearRegressionUI() {
        ctrl = new LinearRegressionController();
    }

    public void mainMenu() {
        boolean changeLevelsValues = menuLevels();
        if (changeLevelsValues) {
            menuChangeLevels();
        }
        int optionDaysOrWeeks, numberOfHistoricalPoints, optionIndependentVariable, optionIndependentVariable2;

//        ctrl.exemploTPS();
        do {
            Date startingDate = LinearRegressionUI.menuStartingDate();
            Date endingDate = LinearRegressionUI.menuEndingDate();
            optionDaysOrWeeks = menuDaysOrWeeks();
            switch (optionDaysOrWeeks) {
                case 1:
                    optionIndependentVariable = menuIndependentVariable();
                    switch (optionIndependentVariable) {
                        case 1:
                            numberOfHistoricalPoints = menuNumberOfHistoricalPoints();
                            ctrl.numberOfTestsPerDay(numberOfHistoricalPoints, startingDate, endingDate);
                            break;
                        case 2:
                            numberOfHistoricalPoints = menuNumberOfHistoricalPoints();
                            ctrl.meanAgePerDay(numberOfHistoricalPoints, startingDate, endingDate);
                            break;
                        case 0:
                            optionDaysOrWeeks = 0;
                            break;
                        default:
                            System.out.println("Invalid Option. Try Again.");
                            break;
                    } while (optionIndependentVariable != 0);
                    break;
                case 2:
                    optionIndependentVariable2 = menuIndependentVariable();
                    switch (optionIndependentVariable2) {
                        case 1:
                            numberOfHistoricalPoints = menuNumberOfHistoricalPoints();
                            ctrl.numberOfTestsPerWeek(numberOfHistoricalPoints, startingDate, endingDate);
                            break;
                        case 2:
                            numberOfHistoricalPoints = menuNumberOfHistoricalPoints();
                            ctrl.meanAgePerWeek(numberOfHistoricalPoints, startingDate, endingDate);
                            break;
                        case 0:
                            optionDaysOrWeeks = 0;
                            break;
                        default:
                            System.out.println("Invalid Option. Try Again.");
                            break;
                    } while (optionIndependentVariable2 != 0);
                    break;
                case 0:
                    System.out.println("End");
                    break;
                default:
                    System.out.println("Invalid Option. Try Again.");
                    break;
            }

        } while (optionDaysOrWeeks != 0);
    }

    public static boolean menuLevels() {
        ConstantsLinearRegression constantsLinearRegression = new ConstantsLinearRegression();
        String menu = "\nHypothesis Tests Significance (Alpha): " + ConstantsLinearRegression.HYPOTHESIS_TESTS_SIGNIFICANCE_LEVEL
                + "\nConfidence Interval: " + ConstantsLinearRegression.CONFIDENCE_LEVEL
                + "\n-- Want to Change Values? (s/n)  --";
        return Utils.confirm(menu);
    }

    public static void menuChangeLevels() {
        String hypothesisTestsSignificanceText = "\n-- Hypothesis Tests Significance (0-100): --";
        int hypothesisTestsSignificance = Utils.readIntegerFromConsole(hypothesisTestsSignificanceText);
        while (hypothesisTestsSignificance < 0 && hypothesisTestsSignificance > 100){
            hypothesisTestsSignificance = Utils.readIntegerFromConsole(hypothesisTestsSignificanceText);
        }
        ConstantsLinearRegression.setHypothesisTestsSignificanceLevel(hypothesisTestsSignificance / 100.0);

        String confidenceIntervalText = "\n-- Confidence Interval Significance (0-100): --";
        int confidenceInterval = Utils.readIntegerFromConsole(confidenceIntervalText);
        while (confidenceInterval < 0 && confidenceInterval > 100){
            confidenceInterval = Utils.readIntegerFromConsole(confidenceIntervalText);
        }
        ConstantsLinearRegression.setConfidenceLevel(confidenceInterval / 100.0);
    }

    public static Date menuStartingDate() {
        System.out.println("NOTE: Interval of Dates will fit the Linear Regression Model");
        Date startingDate = Utils.readDateFromConsole("Enter Starting Date (DD-MM-YYYY): ");
        return startingDate;
    }

    public static Date menuEndingDate() {
        Date endingDate = Utils.readDateFromConsole("Enter Ending Date (DD-MM-YYYY): ");
        return endingDate;
    }

    public static int menuDaysOrWeeks() {
        String menu = "-- Choose Days or Weeks--"
                + "\n1 - Days"
                + "\n2 - Weeks"
                + "\n0 - End Program"
                + "\n-- Choose an option --";
        return Utils.readIntegerFromConsole(menu);
    }

    public static int menuNumberOfHistoricalPoints() {
        System.out.println("NOTE: Number of Historical Points will set the Number of Days/Week to Predict from the Last Day/Week in the Data");
        String text = "-- Choose Number of Historical Points --";
        return Utils.readIntegerFromConsole(text);
    }

    public static int menuIndependentVariable() {
        String menu = "-- Choose Independent Variable--"
                + "\n1 - Number of Tests"
                + "\n2 - Mean Age"
                + "\n0 - End Program"
                + "\n-- Choose an option --";
        return Utils.readIntegerFromConsole(menu);
    }

}
