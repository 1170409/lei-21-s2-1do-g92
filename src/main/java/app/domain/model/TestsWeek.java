package app.domain.model;

import java.util.ArrayList;
import java.util.List;

public class TestsWeek {

    public int weekOfTheYear;

    public List<Test> testsDate;

    public TestsWeek(int paramWeekOfTheYear) {
        weekOfTheYear = paramWeekOfTheYear;
        testsDate = new ArrayList<>();
    }

}
