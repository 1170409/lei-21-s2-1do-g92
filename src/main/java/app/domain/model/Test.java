package app.domain.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

public class Test {

    public String codeTest;

    public String codeNHS;

    public String idLab;

    public String cardNumber;

    public String numberNHS;

    public String birthday;

    public int age;

    public String typeTest;

    public String category;

    public double igGAN;

    public Date testRegDateHour;

    public Test(String paramCodeTest, String paramCodeNHS, String paramIdLab, String paramCardNumber,
                String paramNumberNHS, String paramBirthday, String paramTypeTest, String paramCategory,
                String paramIgGAN, String paramTestRegDateHour) {
        this.codeTest = paramCodeTest;
        this.codeNHS = paramCodeNHS;
        this.idLab = paramIdLab;
        this.cardNumber = paramCardNumber;
        this.numberNHS = paramNumberNHS;
        this.birthday = paramBirthday;
        this.age = calculateAge(birthdayToDate(this.birthday));
        this.typeTest = paramTypeTest;
        this.category = paramCategory;
        this.igGAN = Double.parseDouble(paramIgGAN.replace(",", "."));
        this.testRegDateHour = toDate(paramTestRegDateHour);
    }

    public Date birthdayToDate(String birthday) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(birthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Date toDate(String testRegDateHour) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(testRegDateHour);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isPositive(){
        return igGAN > 1.4;
    }

    public int calculateAge(Date birthday) {
        LocalDate birthDate = birthday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate currentDate = LocalDate.now();
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }

    public String toString() {
        return codeTest + " " + codeNHS + " " + idLab + " " + cardNumber + " " + numberNHS + " " + " " +
                birthday + " " + typeTest + " " + category + " " + igGAN + " " + testRegDateHour;
    }

}
