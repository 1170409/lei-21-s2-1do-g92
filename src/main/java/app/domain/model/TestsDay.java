package app.domain.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestsDay {

    public Date date;

    public List<Test> testsDate;

    public TestsDay(Date paramDate) {
        date = paramDate;
        testsDate = new ArrayList<>();
    }

}
