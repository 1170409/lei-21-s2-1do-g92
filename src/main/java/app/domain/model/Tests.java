package app.domain.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Tests {

    private List<Test> allTests;

    public Tests() {
        allTests = new ArrayList<>();
    }

    public List<Test> getAllTests() {
        return allTests;
    }

    public List<Test> getAllTestsFromDay(Date paramDate) {
        List<Test> testsFromDate = new ArrayList<>();
        TestsDay testsIntervalDates = new TestsDay(paramDate);

        for (Test test : allTests) {

            Calendar paramCalendar = Calendar.getInstance();
            paramCalendar.setTime(paramDate);
            Calendar testCalendar = Calendar.getInstance();
            testCalendar.setTime(test.testRegDateHour);

            if (paramCalendar.get(Calendar.MONTH) == testCalendar.get(Calendar.MONTH)
                    && paramCalendar.get(Calendar.DAY_OF_MONTH) == testCalendar.get(Calendar.DAY_OF_MONTH)) {
                testsFromDate.add(test);
                testsIntervalDates.testsDate.add(test);
            }

        }
        return testsFromDate;
    }

    public List<Test> getAllTestsFromWeek(int weekOfTheYear) {
        List<Test> testsFromDate = new ArrayList<>();
        TestsWeek testsIntervalDates = new TestsWeek(weekOfTheYear);

        for (Test test : allTests) {

            Calendar testCalendar = Calendar.getInstance();
            testCalendar.setTime(test.testRegDateHour);
            int testtWeekOfTheYear = testCalendar.get(Calendar.WEEK_OF_YEAR);


            if (testtWeekOfTheYear == weekOfTheYear) {
                testsFromDate.add(test);
                testsIntervalDates.testsDate.add(test);
            }

        }
        return testsFromDate;
    }
}
