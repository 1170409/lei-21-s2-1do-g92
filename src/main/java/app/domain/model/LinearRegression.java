package app.domain.model;

import app.domain.shared.ConstantsLinearRegression;
import org.apache.commons.math3.distribution.FDistribution;

public class LinearRegression {
    public double intercept, slope;
    public double r2, r, r2Adjusted;
    public double svar, svar0, svar1;
    public double xbar, ybar;
    public double xxbar, yybar, xybar;
    public double rss, ssr;
    public double averageX, averageY;
    public double interceptStdErr, slopeStdErr;
    public double[] fit;
    public double[] residuals;
    public double[] x;
    public double[] y;
    public int n, degreesOfFreedom;

    public LinearRegression(double[] x, double[] y) {
        if (x.length != y.length) {
            throw new IllegalArgumentException("array lengths are not equal");
        }
        this.x = x;
        this.y = y;
        n = x.length;

        averageX = 0.0;
        averageY = 0.0;
        for (int i = 0; i < x.length; i++) {
            averageX += x[i];
            averageY += y[i];
        }
        averageX /= n;
        averageY /= n;

        // first pass
        double sumx = 0.0, sumy = 0.0, sumx2 = 0.0;
        for (int i = 0; i < n; i++) {
            sumx += x[i];
            sumx2 += x[i] * x[i];
            sumy += y[i];
        }
        xbar = sumx / n;
        ybar = sumy / n;

        // second pass: compute summary statistics
        xxbar = 0.0;
        yybar = 0.0;
        xybar = 0.0;
        for (int i = 0; i < n; i++) {
            xxbar += (x[i] - xbar) * (x[i] - xbar);
            yybar += (y[i] - ybar) * (y[i] - ybar);
            xybar += (x[i] - xbar) * (y[i] - ybar);
        }
        slope = xybar / xxbar;
        intercept = ybar - slope * xbar;

        // more statistical analysis
        rss = 0.0;      // residual sum of squares
        ssr = 0.0;      // regression sum of squares

        fit = new double[n];
        residuals = new double[n];
        for (int i = 0; i < n; i++) {
            fit[i] = slope * x[i] + intercept;
            residuals[i] = (y[i] - fit[i]);
            rss += (fit[i] - y[i]) * (fit[i] - y[i]);
            ssr += (fit[i] - ybar) * (fit[i] - ybar);
        }

        degreesOfFreedom = n - 2;
        r2 = ssr / yybar;
        r = Math.sqrt(r2);
        r2Adjusted = 1 - (((1-r2) * (n-1)) / (n-2));
        svar = rss / degreesOfFreedom;
        svar1 = svar / xxbar;
        svar0 = svar / n + xbar * xbar * svar1;
    }

    public double intercept() {
        return intercept;
    }

    public double slope() {
        return slope;
    }

    public double R2() {
        return r2;
    }

    public double R2Adjusted() {
        return r2Adjusted;
    }

    public double R() {
        return r;
    }

    public double interceptStdErr() {
        interceptStdErr = Math.sqrt(svar0);
        return interceptStdErr;
    }

    public double slopeStdErr() {
        slopeStdErr = Math.sqrt(svar1);
        return slopeStdErr;
    }

    public double predict(double x) {
        return slope * x + intercept;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%.2f n + %.2f", slope(), intercept()));
        stringBuilder.append("  (R^2 = " + String.format("%.3f", R2()) + ")");
        return stringBuilder.toString();
    }

}
