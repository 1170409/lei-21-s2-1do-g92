package app.domain.model;

import app.domain.shared.ConstantsLinearRegression;
import org.apache.commons.math3.distribution.FDistribution;
import org.apache.commons.math3.distribution.TDistribution;

import java.util.List;

public class LinearRegressionWeek extends LinearRegression{

    public double[] xHistoricalPoints;

    public double[] yHistoricalPoints;

    public List<String> allWeeks;

    public LinearRegressionWeek(double[] x, double[] y, double[] paramXHistoricalPoints, double[] paramYHistoricalPoints, List<String> paramAllWeeks) {
        super(x, y);
        xHistoricalPoints = paramXHistoricalPoints;
        yHistoricalPoints = paramYHistoricalPoints;
        allWeeks = paramAllWeeks;
    }

    public void printStatistics(){
        System.out.format("Regression Model: Y = " + "%f X + %f\n", slope, intercept);
        System.out.println("\nOther Statistics\n");
        System.out.format("R2 = " + "%f\n", r2);
        System.out.format("R2adjusted = " + "%f\n", r2Adjusted);
        System.out.format("R = " + "%f\n", r);
        System.out.format("-------------------------------------------------------------------------------------\n");
    }

    public void printHypothesisTestsForRegressionCoefficientsIntercept(){

        System.out.println("\nHypothesis Tests for Regression Coefficients\n");
        System.out.println("H0: a=0  H1:a<>0\n");
        System.out.println("Decision: ");

        TDistribution td = new TDistribution(this.degreesOfFreedom);

        double alphaTD = ConstantsLinearRegression.HYPOTHESIS_TESTS_SIGNIFICANCE_LEVEL;
        double critAlphaTD = 1.0 - alphaTD/2.0;
        double critTD;
        if(alphaTD > 0.5) {
            critTD = td.inverseCumulativeProbability(critAlphaTD);

        } else {
            critTD = td.inverseCumulativeProbability(critAlphaTD);
        }

        double SE = yybar - Math.pow(slope,2)*xxbar;
        double S = Math.sqrt((1.0/degreesOfFreedom) * SE);
        double E = Math.sqrt((1.0/n) + (Math.pow((averageX),2)/xxbar));

        double tObs1 = this.intercept / (S * E);
        double tObs = Math.abs(tObs1);

        if (tObs > critTD) {
            System.out.format("tObs > t%.4f, (" + "%.4f > %.4f" + ")\n", critAlphaTD, tObs, critTD);
            System.out.println("REJECT H0 => (a != 0)\n");
        } else {
            System.out.format("tObs <= t%.4f, (" + "%.4f <= %.4f" + ")\n", critAlphaTD, tObs, critTD);
            System.out.println("DON'T REJECT H0 => (a == 0)\n");
        }
    }

    public void printHypothesisTestsForRegressionCoefficientsSlope(){

        System.out.println("H0: b=0  H1:b<>0\n");
        System.out.println("Decision: ");

        TDistribution td = new TDistribution(this.degreesOfFreedom);

        double alphaTD = ConstantsLinearRegression.HYPOTHESIS_TESTS_SIGNIFICANCE_LEVEL;
        double critAlphaTD = 1.0 - alphaTD/2.0;
        double critTD;
        if(alphaTD > 0.5) {
            critTD = td.inverseCumulativeProbability(critAlphaTD);

        } else {
            critTD = td.inverseCumulativeProbability(critAlphaTD);
        }

        double SE = yybar - Math.pow(slope,2)*xxbar;
        double S = Math.sqrt((1.0/degreesOfFreedom) * SE);
        double E = Math.sqrt(xxbar);

        double tObs1 = this.slope / (S / E);
        double tObs = Math.abs(tObs1);

        if (tObs > critTD) {
            System.out.format("tObs > t%.4f, (" + "%.4f > %.4f" + ")\n", critAlphaTD, tObs, critTD);
            System.out.println("REJECT H0 => (b != 0)\n");
        } else {
            System.out.format("tObs <= t%.4f, (" + "%.4f <= %.4f" + ")\n", critAlphaTD, tObs, critTD);
            System.out.println("DON'T REJECT H0 => (b == 0)\n");
        }
        System.out.format("-------------------------------------------------------------------------------------\n");
    }

    public void printAnova(){
        System.out.println("\nSignificance Model with Anova");
        System.out.println("H0: b=0  H1:b<>0\n");
        final Object[][] table = new String[4][5];
        table[0] = new String[] {"", "df", "SS", "MS", "F"};
        double fObs = ssr/svar;
        table[1] = new String[] {"Regression", String.valueOf(1), String.valueOf(ssr), String.valueOf(ssr), String.valueOf(fObs)};
        table[2] = new String[] {"Residual", String.valueOf(n-2), String.valueOf(rss), String.valueOf(svar), ""};
        table[3] = new String[] {"Total", String.valueOf(n-1), String.valueOf(yybar), "", ""};

        for (final Object[] row : table) {
            System.out.format("%35s%35s%35s%35s%35s%n", row);
        }

        System.out.println("Decision: f");

        FDistribution fd = new FDistribution(1,degreesOfFreedom);

        double alphaFD = ConstantsLinearRegression.HYPOTHESIS_TESTS_SIGNIFICANCE_LEVEL;
        double critFD = fd.inverseCumulativeProbability(1 - alphaFD);
        System.out.format("0 > f%.2f, (" + "%d.%d" + ") = %f\n", alphaFD, 1, degreesOfFreedom, critFD);
        if (fObs > critFD) {
            System.out.println("Reject H0 => (The Regression Model is Significant)");
        } else {
            System.out.println("Accept H0 => (The Regression Model is NOT Significant)");
        }
        System.out.format("-------------------------------------------------------------------------------------\n");
    }

    public void printPredictions(){
        System.out.println("Prediction Values:");

        final Object[][] tablePrediction = new String[xHistoricalPoints.length+1][4];
        String intervals = String.format("%.2f intervals", ConstantsLinearRegression.CONFIDENCE_LEVEL*100);
        tablePrediction[0] = new String[] {"Week of the Year", "Number of OBSERVED positive cases", "Number of ESTIMATED positive cases", intervals};

        TDistribution td = new TDistribution(degreesOfFreedom);
        double alphaTD = 1 - ConstantsLinearRegression.CONFIDENCE_LEVEL;
        double critAlphaTD = 1.0 - alphaTD/2.0;

        int i = 1;
        while (i <= xHistoricalPoints.length){
            for (double x0 : this.xHistoricalPoints) {
                double prediction = predict(x0);

                double critTD;
                if(alphaTD > 0.5) {
                    critTD = td.inverseCumulativeProbability(critAlphaTD);
                } else {
                    critTD = td.inverseCumulativeProbability(critAlphaTD);
                }

                double SE = yybar - Math.pow(slope,2)*xxbar;
                double S = Math.sqrt((1.0/degreesOfFreedom) * SE);
                double E = Math.sqrt((1.0) + (1.0/n) + (Math.pow((x0-averageX),2)/xxbar));
                double delta = critTD * S * E;
                double limInf = prediction - delta;
                double limSup = prediction + delta;
                tablePrediction[i] = new String[] {allWeeks.get(i-1), String.valueOf(yHistoricalPoints[i-1]), String.valueOf(prediction),String.format("%.02f", limInf) + " - " + String.format("%.02f", limSup)};
                i++;
            }
        }
        for (final Object[] rowPrediction : tablePrediction) {
            System.out.format("%50s%50s%50s%50s%n", rowPrediction);
        }
    }

    public void printReport(){
        printStatistics();
        printHypothesisTestsForRegressionCoefficientsIntercept();
        printHypothesisTestsForRegressionCoefficientsSlope();
        printAnova();
        printPredictions();
    }
}
