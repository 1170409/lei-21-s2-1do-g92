package app.domain.shared;

public class ConstantsLinearRegression {

    public static double HYPOTHESIS_TESTS_SIGNIFICANCE_LEVEL;
    public static double CONFIDENCE_LEVEL;

    public ConstantsLinearRegression(){
        ConstantsLinearRegression.setHypothesisTestsSignificanceLevel(0.05);
        ConstantsLinearRegression.setConfidenceLevel(0.95);
    }

    public static double getHypothesisTestsSignificanceLevel() {
        return HYPOTHESIS_TESTS_SIGNIFICANCE_LEVEL;
    }

    public static void setHypothesisTestsSignificanceLevel(double hypothesisTestsSignificanceLevel) {
        HYPOTHESIS_TESTS_SIGNIFICANCE_LEVEL = hypothesisTestsSignificanceLevel;
    }

    public static double getConfidenceLevel() {
        return CONFIDENCE_LEVEL;
    }

    public static void setConfidenceLevel(double confidenceLevel) {
        CONFIDENCE_LEVEL = confidenceLevel;
    }

}
