package app.controller;

import app.domain.model.Test;
import app.domain.model.Tests;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadCSVFileController {

    public String fileName;

    public ReadCSVFileController(String paramFileName) {
        this.fileName = paramFileName;
    }

    public Tests loadTests() {
        String line;
        String splitBy = ";";
        Tests allTests = new Tests();
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            int iteration = 0;
            while ((line = br.readLine()) != null) {
                if (iteration == 0) {
                    iteration++;
                    continue;
                }
                String[] splittedLine = line.split(splitBy);

                Test test = new Test(splittedLine[0], splittedLine[1], splittedLine[2], splittedLine[3], splittedLine[4], splittedLine[6], splittedLine[11], splittedLine[19], splittedLine[20], splittedLine[21]);
                allTests.getAllTests().add(test);
//                System.out.println(test);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allTests;
    }

}
