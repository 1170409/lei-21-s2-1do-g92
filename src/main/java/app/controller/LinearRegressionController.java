package app.controller;

import app.domain.model.*;
import app.domain.shared.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class LinearRegressionController {

    private Tests allTests;

    public LinearRegressionController() {
        ReadCSVFileController readCSVFileController = new ReadCSVFileController("src/main/java/resources/bootstrap/tests_CovidMATCPCSV.csv");
        allTests = readCSVFileController.loadTests();
    }

    public void numberOfTestsPerDay(int numberOfHistoricalPoints, Date startingDate, Date endingDate) {
        // WILL BE USED TO FIT THE LINEAR REGRESSION MODEL
        List<TestsDay> testsInTheDateInterval = getAllTestsInTheDateIntervalPerDay(startingDate, endingDate);
        int numberOfDaysInTheDateInterval = testsInTheDateInterval.size();
        double totalTestsInTheDateInterval[] = new double[numberOfDaysInTheDateInterval];
        double positiveTestsInTheDateInterval[] = new double[numberOfDaysInTheDateInterval];
        getTotalAndPositiveTestsPerDay(testsInTheDateInterval, totalTestsInTheDateInterval, positiveTestsInTheDateInterval);

        // WILL BE USED FOR THE PREDICTIONS
        List<TestsDay> testsDaysHistoricalPoints = getDaysFromHistoricalPoints(numberOfHistoricalPoints);
        List<String> allDays = getAllDays(testsDaysHistoricalPoints);
        int numberOfDaysHistoricalPoints = testsDaysHistoricalPoints.size();
        double totalTestsHistoricalPoints[] = new double[numberOfDaysHistoricalPoints];
        double positiveTestsHistoricalPoints[] = new double[numberOfDaysHistoricalPoints];
        getTotalAndPositiveTestsPerDay(testsDaysHistoricalPoints, totalTestsHistoricalPoints, positiveTestsHistoricalPoints);

        // REPORT LINEAR REGRESSION
        LinearRegressionDay linearRegression = new LinearRegressionDay(totalTestsInTheDateInterval,positiveTestsInTheDateInterval,totalTestsHistoricalPoints,positiveTestsHistoricalPoints,allDays);
        linearRegression.printReport();
    }

    public void meanAgePerDay(int numberOfHistoricalPoints, Date startingDate, Date endingDate) {
        // WILL BE USED TO FIT THE LINEAR REGRESSION MODEL
        List<TestsDay> testsInTheDateInterval = getAllTestsInTheDateIntervalPerDay(startingDate, endingDate);
        int numberOfDaysInTheDateInterval = testsInTheDateInterval.size();
        double meanAgeInTheDateInterval[] = new double[numberOfDaysInTheDateInterval];
        double positiveTestsInTheDateInterval[] = new double[numberOfDaysInTheDateInterval];
        getMeanAgeAndPositiveTestsPerDay(testsInTheDateInterval, meanAgeInTheDateInterval, positiveTestsInTheDateInterval);

        // WILL BE USED FOR THE PREDICTIONS
        List<TestsDay> testsDaysHistoricalPoints = getDaysFromHistoricalPoints(numberOfHistoricalPoints);
        List<String> allDays = getAllDays(testsDaysHistoricalPoints);
        int numberOfDaysHistoricalPoints = testsDaysHistoricalPoints.size();
        double meanAgeHistoricalPoints[] = new double[numberOfDaysHistoricalPoints];
        double positiveTestsHistoricalPoints[] = new double[numberOfDaysHistoricalPoints];
        getMeanAgeAndPositiveTestsPerDay(testsDaysHistoricalPoints, meanAgeHistoricalPoints, positiveTestsHistoricalPoints);

        // REPORT LINEAR REGRESSION
        LinearRegressionDay linearRegression = new LinearRegressionDay(meanAgeInTheDateInterval,positiveTestsInTheDateInterval,meanAgeHistoricalPoints,positiveTestsHistoricalPoints,allDays);
        linearRegression.printReport();
    }

    public void numberOfTestsPerWeek(int numberOfHistoricalPoints, Date startingDate, Date endingDate) {
        // WILL BE USED TO FIT THE LINEAR REGRESSION MODEL
        List<TestsWeek> testsInTheDateInterval = getAllTestsInTheDateIntervalPerWeek(startingDate, endingDate);
        int numberOfWeeksInTheDateInterval = testsInTheDateInterval.size();
        double totalTestsInTheDateInterval[] = new double[numberOfWeeksInTheDateInterval];
        double positiveTestsInTheDateInterval[] = new double[numberOfWeeksInTheDateInterval];
        getTotalAndPositiveTestsPerWeek(testsInTheDateInterval, totalTestsInTheDateInterval, positiveTestsInTheDateInterval);

        // WILL BE USED FOR THE PREDICTIONS
        List<TestsWeek> testsWeeksHistoricalPoints = getWeeksFromHistoricalPoints(numberOfHistoricalPoints);
        List<String> allWeeks = getAllWeeks(testsWeeksHistoricalPoints);
        int numberOfWeeksHistoricalPoints = testsWeeksHistoricalPoints.size();
        double totalTestsHistoricalPoints[] = new double[numberOfWeeksHistoricalPoints];
        double positiveTestsHistoricalPoints[] = new double[numberOfWeeksHistoricalPoints];
        getTotalAndPositiveTestsPerWeek(testsWeeksHistoricalPoints, totalTestsHistoricalPoints, positiveTestsHistoricalPoints);

        // REPORT LINEAR REGRESSION
        LinearRegressionWeek linearRegression = new LinearRegressionWeek(totalTestsInTheDateInterval,positiveTestsInTheDateInterval,totalTestsHistoricalPoints,positiveTestsHistoricalPoints,allWeeks);
        linearRegression.printReport();
    }

    public void meanAgePerWeek(int numberOfHistoricalPoints, Date startingDate, Date endingDate) {
        // WILL BE USED TO FIT THE LINEAR REGRESSION MODEL
        List<TestsWeek> testsInTheDateInterval = getAllTestsInTheDateIntervalPerWeek(startingDate, endingDate);
        int numberOfWeeksInTheDateInterval = testsInTheDateInterval.size();
        double totalTestsInTheDateInterval[] = new double[numberOfWeeksInTheDateInterval];
        double positiveTestsInTheDateInterval[] = new double[numberOfWeeksInTheDateInterval];
        getMeanAgeAndPositiveTestsPerWeek(testsInTheDateInterval, totalTestsInTheDateInterval, positiveTestsInTheDateInterval);

        // WILL BE USED FOR THE PREDICTIONS
        List<TestsWeek> testsWeeksHistoricalPoints = getWeeksFromHistoricalPoints(numberOfHistoricalPoints);
        List<String> allWeeks = getAllWeeks(testsWeeksHistoricalPoints);
        int numberOfWeeksHistoricalPoints = testsWeeksHistoricalPoints.size();
        double totalTestsHistoricalPoints[] = new double[numberOfWeeksHistoricalPoints];
        double positiveTestsHistoricalPoints[] = new double[numberOfWeeksHistoricalPoints];
        getMeanAgeAndPositiveTestsPerWeek(testsWeeksHistoricalPoints, totalTestsHistoricalPoints, positiveTestsHistoricalPoints);

        // REPORT LINEAR REGRESSION
        LinearRegressionWeek linearRegression = new LinearRegressionWeek(totalTestsInTheDateInterval,positiveTestsInTheDateInterval,totalTestsHistoricalPoints,positiveTestsHistoricalPoints,allWeeks);
        linearRegression.printReport();
    }

    public List<TestsDay> getDaysFromHistoricalPoints(int numberOfHistoricalPoints) {
        List<TestsDay> testsDaysHistoricalPoints = new ArrayList<>();

        // GETS DATE OF OLDER TEST
        List<Date> listOfAllDates = new ArrayList<>();
        for (Test test : allTests.getAllTests()) {
            listOfAllDates.add(test.testRegDateHour);
        }
        Date olderDate = Collections.max(listOfAllDates); // LAST DAY HISTORICAL POINTS

        Date currentDate = olderDate;
        int i = 0;
        while (i < numberOfHistoricalPoints) {
            Calendar currentCalendar = Calendar.getInstance();
            currentCalendar.setTime(currentDate);
            List<Test> testsFromDate = allTests.getAllTestsFromDay(currentDate);// ALL TESTS IN THE CURRENT DATE
            if (testsFromDate.size() == 0) {// IF CURRENT DATE DOES NOT HAVE TESTS IS NOT ADDED - SKIPS TO THE NEXT DAY
                currentCalendar.add(Calendar.DATE, -1);
                currentDate = currentCalendar.getTime();
                continue;
            }
            TestsDay testsDate = new TestsDay(currentDate);
            testsDate.testsDate = testsFromDate;
            testsDaysHistoricalPoints.add(testsDate);

            currentCalendar.add(Calendar.DATE, -1);
            currentDate = currentCalendar.getTime();

            i++;
        }
        return testsDaysHistoricalPoints;
    }

    public List<TestsWeek> getWeeksFromHistoricalPoints(int numberOfHistoricalPoints) {
        List<TestsWeek> testsWeeksHistoricalPoints = new ArrayList<>();

        // GETS DATE OF OLDER TEST
        List<Date> listOfAllDates = new ArrayList<>();
        for (Test test : allTests.getAllTests()) {
            listOfAllDates.add(test.testRegDateHour);
        }
        Date olderDate = Collections.max(listOfAllDates); // LAST DAY HISTORICAL POINTS

        Date currentDate = olderDate;
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTime(currentDate);
        int currentWeekOfTheYear = currentCalendar.get(Calendar.WEEK_OF_YEAR);

        int i = 0;
        while (i < numberOfHistoricalPoints) {
            List<Test> testsFromDate = allTests.getAllTestsFromWeek(currentWeekOfTheYear);// ALL TESTS IN THE CURRENT WEEK
            if (testsFromDate.size() == 0) {// IF CURRENT DATE DOES NOT HAVE TESTS IS NOT ADDED - SKIPS TO THE NEXT WEEK
                currentWeekOfTheYear--;
                continue;
            }

            TestsWeek testsDate = new TestsWeek(currentWeekOfTheYear);
            testsDate.testsDate = testsFromDate;
            testsWeeksHistoricalPoints.add(testsDate);

            currentWeekOfTheYear--;

            i++;
        }
        return testsWeeksHistoricalPoints;
    }

    public List<TestsDay> getAllTestsInTheDateIntervalPerDay(Date startingDate, Date endingDate) {
        List<TestsDay> testsInTheDateInterval = new ArrayList<>();

        Date currentDate = startingDate;
        while (currentDate.before(endingDate) || Utils.isSameDate(currentDate,endingDate)) {
            Calendar currentCalendar = Calendar.getInstance();
            currentCalendar.setTime(currentDate);

            List<Test> testsFromDate = allTests.getAllTestsFromDay(currentDate);// ALL TESTS IN THE CURRENT DATE
            if (testsFromDate.size() == 0) {// IF CURRENT DATE DOES NOT HAVE TESTS IS NOT ADDED - SKIPS TO THE NEXT DAY
                currentCalendar.add(Calendar.DATE, 1);
                currentDate = currentCalendar.getTime();
                continue;
            }

            TestsDay testsDate = new TestsDay(currentDate);
            testsDate.testsDate = testsFromDate;
            testsInTheDateInterval.add(testsDate);

            currentCalendar.add(Calendar.DATE, 1);
            currentDate = currentCalendar.getTime();
        }
        return testsInTheDateInterval;
    }

    public List<TestsWeek> getAllTestsInTheDateIntervalPerWeek(Date startingDate, Date endingDate) {
        List<TestsWeek> testsInTheDateIntervalPerWeek = new ArrayList<>();

        Calendar startingCalendar = Calendar.getInstance();
        startingCalendar.setTime(startingDate);
        int startingWeekOfTheYear = startingCalendar.get(Calendar.WEEK_OF_YEAR);

        Calendar endingCalendar = Calendar.getInstance();
        endingCalendar.setTime(endingDate);
        int endingWeekOfTheYear = endingCalendar.get(Calendar.WEEK_OF_YEAR);

        int currentWeekOfTheYear = startingWeekOfTheYear;
        while (currentWeekOfTheYear <= endingWeekOfTheYear) {
            List<Test> testsFromDate = allTests.getAllTestsFromWeek(currentWeekOfTheYear);// ALL TESTS IN THE CURRENT WEEK
            if (testsFromDate.size() == 0) {// IF CURRENT DATE DOES NOT HAVE TESTS IS NOT ADDED - SKIPS TO THE NEXT DAY
                currentWeekOfTheYear++;
                continue;
            }

            TestsWeek testsDate = new TestsWeek(currentWeekOfTheYear);
            testsDate.testsDate = testsFromDate;
            testsInTheDateIntervalPerWeek.add(testsDate);

            currentWeekOfTheYear++;
        }
        return testsInTheDateIntervalPerWeek;
    }

    public void getTotalAndPositiveTestsPerDay(List<TestsDay> testsInTheDateInterval, double totalTests[], double positiveTests[]) {
        int j = 0, counterPositiveTests = 0;
        for (TestsDay testsDate : testsInTheDateInterval) {
            totalTests[j] = testsDate.testsDate.size();
            for (Test test : testsDate.testsDate) {
                if (test.isPositive()){
                    counterPositiveTests++;
                }
            }
            positiveTests[j] = counterPositiveTests;
            j++;
            counterPositiveTests = 0;
        }
    }

    public void getTotalAndPositiveTestsPerWeek(List<TestsWeek> testsInTheDateInterval, double totalTests[], double positiveTests[]) {
        int j = 0, counterPositiveTests = 0;
        for (TestsWeek testsDate : testsInTheDateInterval) {
            totalTests[j] = testsDate.testsDate.size();
            for (Test test : testsDate.testsDate) {
                if (test.isPositive()){
                    counterPositiveTests++;
                }
            }
            positiveTests[j] = counterPositiveTests;
            j++;
            counterPositiveTests = 0;
        }
    }

    public void getMeanAgeAndPositiveTestsPerDay(List<TestsDay> testsInTheDateInterval, double meanAge[], double positiveTests[]) {
        int j = 0, counterPositiveTests = 0;
        double meanAgePerDay = 0.0;
        for (TestsDay testsDate : testsInTheDateInterval) {
            for (Test test : testsDate.testsDate) {
                meanAgePerDay += test.age;
                if (test.isPositive()){
                    counterPositiveTests++;
                }
            }
            meanAgePerDay /= testsDate.testsDate.size();
            meanAge[j] = meanAgePerDay;
            positiveTests[j] = counterPositiveTests;
            j++;
            meanAgePerDay = 0.0;
            counterPositiveTests = 0;
        }
    }

    public void getMeanAgeAndPositiveTestsPerWeek(List<TestsWeek> testsInTheDateInterval, double meanAge[], double positiveTests[]) {
        int j = 0, counterPositiveTests = 0;
        double meanAgePerWeek = 0.0;
        for (TestsWeek testsDate : testsInTheDateInterval) {
            for (Test test : testsDate.testsDate) {
                meanAgePerWeek += test.age;
                if (test.isPositive()){
                    counterPositiveTests++;
                }
            }
            meanAgePerWeek /= testsDate.testsDate.size();
            meanAge[j] = meanAgePerWeek;
            positiveTests[j] = counterPositiveTests;
            j++;
            meanAgePerWeek = 0.0;
            counterPositiveTests = 0;
        }
    }

    public List<String> getAllDays(List<TestsDay> testsInTheDateInterval) {
        List<String> allDays = new ArrayList<>();
        String pattern = "dd/MM";
        DateFormat df = new SimpleDateFormat(pattern);
        for (TestsDay testsDate : testsInTheDateInterval) {
            String dateAsString = df.format(testsDate.date);
            allDays.add(dateAsString);
        }
        return allDays;
    }

    public List<String> getAllWeeks(List<TestsWeek> testsInTheDateInterval) {
        List<String> allWeeks = new ArrayList<>();
        for (TestsWeek testsDate : testsInTheDateInterval) {
            allWeeks.add(String.valueOf(testsDate.weekOfTheYear));
        }
        return allWeeks;
    }
}
